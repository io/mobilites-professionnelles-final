# Mobilités professionnelles : statistiques nationales de l'INSEE

V1 complète 2022-08-18

## Présentation du Notebook

Démarche d'analyse de données statistiques et géographiques

L'objectif est de présenter un traitement de données sur les mobilités professionnelles des français à partir des données disponibles, avec un focus sur les données géographiques.

Les données utilisées sont celles de l'INSEE issues du recensement ainsi que des données cartographies disponibles en accès libre sur OpenStreetMap.

Niveau du Notebook : débutant/intermédiaire

Principales notions vues :

- manipulation de grands tableaux
- statistiques exploratoires
- données géographiques et visualisation de cartes
- usage d'API
- réduction de dimensionnalité
- classification (K-Means)
- régressions linéaires


## Contenu du notebook

Présentation générale

Contexte et données

1. Chargement des données

2. Statistiques sur les déplacements à l'échelle des déplacements individuels

2.1. Repérer et recoder les variables

2.2. Statistiques descriptives sur les déplacements des individus

2.3. Ajout d'une nouvelle dimension : la distance parcourue

2.4. Interroger un service de routage pour la distance parcourue

3. Représenter géographiquement les mobilités

4. Étudier les profils des communes

5. Regrouper les communes par proximité de déplacement

5.1. Construire le réseau des trajets

5.2. Visualiser un réseau

5.3. Clusters dans un réseau

5.4. Carte des zones

6. Utiliser la catégorie spatiale dans un modèle statistique

6.1. Analyse descriptive et représentations

6.2. Modèle linéaire multivarié

6.3. Régression spatiale

Conclusion

## Exécution du Notebook

Ce notebook a été conçu pour le noyau Callisto *Deep Learning Python 3.8*.

Un fichier *requirements.txt* contient les bibliothèques nécessaires à l'exécution.

## Crédits

Ce notebook a été réalisé par Émilien Schultz (@EmilienSchultz) et Mathieu Morey.

## Licence

MIT License

Copyright (c) 2022 Huma-Num Lab

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:
The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.
THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
